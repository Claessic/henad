﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Net;
using System;
using System.Text;

public class ChatServer : MonoBehaviour
{

    public string test;

    // Use this for initialization
    void Start()
    {
        ConnectToChatServer(Environment.GetCommandLineArgs(), ref test);
    }


    // Update is called once per frame
    void Update()
    {

    }

    static void ConnectToChatServer(string[] args, ref string test)
    {
        Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
            ProtocolType.Udp);

        //IPAddress broadcast = IPAddress.Parse("192.168.0.255");
        IPAddress broadcast = IPAddress.Parse("127.0.0.1");

        //byte[] sendbuf = Encoding.ASCII.GetBytes(args[0]);
        byte[] sendbuf = Encoding.UTF8.GetBytes("Hey hey hey");

        IPEndPoint ep = new IPEndPoint(broadcast, 11000);

        s.SendTo(sendbuf, ep);
        test = "Well, we made it this far, you and I!";
        Debug.Log("Message sent to the broadcast address");
        
        
    }
}
