﻿using UnityEngine;
using System.Collections;
using System;
using System.Net.Sockets;

public class LobbyServer : MonoBehaviour
{
    
    private static TcpClient client;
    private static NetworkStream stream;
    public string testMessage;
    public string testResponse;
    public GameObject player;
    public int timesSpace = 0;

    // Use this for initialization
    void Start()
    {
        ConnectToLobby(ref client, ref stream, "169.254.86.218", player.transform.position.ToString());
        
    }

    // Update is called once per frame
    void Update()
    {
        SendLobbyMessage(ref client, ref stream, player.transform.position.ToString());

        if (Input.GetKeyDown(KeyCode.R))
        {
            CloseConnection(client, stream);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {

            //ConnectToLobby(client, stream, "127.0.0.1", "Space pressed: " + timesSpace.ToString());
            SendLobbyMessage(ref client, ref stream, "Jeg trykker på space nu!");
            timesSpace++;
        }
    }

    static void ConnectToLobby(ref TcpClient client, ref NetworkStream stream, String server, String message)
    {
        try
        {
            // Create a TcpClient. 
            // Note, for this client to work you need to have a TcpServer  
            // connected to the same address as specified by the server, port 
            // combination.
            Int32 port = 13001;
            client = new TcpClient(server, port);

            // Translate the passed message into ASCII and store it as a Byte array.
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);

            // Get a client stream for reading and writing. 
            //  Stream stream = client.GetStream();

            stream = client.GetStream();

            // Send the message to the connected TcpServer. 
            stream.Write(data, 0, data.Length);

            Console.WriteLine("Sent: {0}", message);

            GameObject.Find("LobbyServer").GetComponent<LobbyServer>().testMessage = System.Text.Encoding.UTF8.GetString(data);

            // Receive the TcpServer.response. 

            // Buffer to store the response bytes.
            data = new Byte[256];

            // String to store the response ASCII representation.
            String responseData = String.Empty;

            // Read the first batch of the TcpServer response bytes.
            Int32 bytes = stream.Read(data, 0, data.Length);
            responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

            GameObject.Find("LobbyServer").GetComponent<LobbyServer>().testResponse = responseData;
            Console.WriteLine("Received: {0}", responseData);
            
        }
        catch (ArgumentNullException e)
        {
            Console.WriteLine("ArgumentNullException: {0}", e);
        }
        catch (SocketException e)
        {
            Console.WriteLine("SocketException: {0}", e);
        }
        finally
        {

        }

        Console.WriteLine("\n Press Enter to continue...");
        Console.Read();
    }

    private void CloseConnection(TcpClient client, NetworkStream stream)
    {
        // Close everything.
        stream.Close();
        client.Close();
    }

    private void SendLobbyMessage(ref TcpClient client, ref NetworkStream stream, string message)
    {
        Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
        // Send the message to the connected TcpServer. 
        stream.Write(data, 0, data.Length);
        
        // Receive the TcpServer.response. 

        // Buffer to store the response bytes.
        data = new Byte[256];

        // String to store the response ASCII representation.
        String responseData = String.Empty;

        // Read the first batch of the TcpServer response bytes.
        Int32 bytes = stream.Read(data, 0, data.Length);
        responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

        GameObject.Find("LobbyServer").GetComponent<LobbyServer>().testResponse = responseData;

        // Write response to debug log
        Debug.Log(responseData);
    }
}
