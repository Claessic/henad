﻿using UnityEngine;
using System.Collections;

public class test : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        this.transform.position = new Vector2(0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-1 * Time.deltaTime, 0, 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(1 * Time.deltaTime, 0, 0);
        }
    }
}
