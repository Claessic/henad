﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LobbyServer
{
    public class ASyncClientRequest
    {
        public string position;
        TcpClient client;
        NetworkStream stream = null;

        public ASyncClientRequest(TcpClient connectedASyncClient)
        {
            this.client = connectedASyncClient;
        }

        public void StartClient()
        {
            stream = client.GetStream();
            WaitForNewRequest();
        }

        public void WaitForNewRequest()
        {
            byte[] buffer = new byte[client.ReceiveBufferSize];
            stream.BeginRead(buffer, 0, buffer.Length, ReadASyncCallback, buffer);
        }

        private void ReadASyncCallback(IAsyncResult result)
        {
            NetworkStream networkStream = client.GetStream();

            try
            {
                int read = networkStream.EndRead(result);
                if (read == 0)
                {
                    stream.Close();
                    client.Close();
                    return;
                }

                byte[] buffer = result.AsyncState as byte[];
                string data = Encoding.UTF8.GetString(buffer, 0, read);

                position = data;
                //Use the data here :D

                //Send data back to client here!

                //Byte[] sendBytes = Encoding.UTF8.GetBytes("Async request handled! Sent back: " + data);
                //Byte[] sendBytes = Encoding.UTF8.GetBytes(data);

                string combinedPlayerData = string.Empty;

                foreach(ASyncClientRequest c in Program.connectedClients)
                {
                    combinedPlayerData = combinedPlayerData + c.position;
                }
                Byte[] sendBytes = Encoding.UTF8.GetBytes(combinedPlayerData);

                networkStream.Write(sendBytes, 0, sendBytes.Length);
                networkStream.Flush();

            }

            catch(Exception e)
            {
                throw e;
            }

            this.WaitForNewRequest();
        }

    }
}
