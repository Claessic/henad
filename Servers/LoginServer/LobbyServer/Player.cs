﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LobbyServer
{
    public class Player
    {
        private bool isAlive;
        private float posX;
        private float posY;
        public TcpClient client;
        public NetworkStream stream;

        public Player(TcpClient client)
        {
            this.client = client;
            this.stream = client.GetStream();
        }
    }
}
