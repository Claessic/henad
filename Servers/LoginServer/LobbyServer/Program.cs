﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LobbyServer
{
    class Program
    {
        private static TcpListener aSyncListener;
        public static List<ASyncClientRequest> connectedClients;
        
        private List<ActiveGame> openGames;
        private List<ActiveGame> closedGames;
        private static List<Player> onlinePlayers;

        static void Main(string[] args)
        {
            onlinePlayers = new List<Player>();
            connectedClients = new List<ASyncClientRequest>();
            //Normal Server
            //StartSyncServer();

            //Async server
            StartAsyncServer();

            //Test
            //StartTest();

            Console.WriteLine("\nServer is running...");
            Console.ReadKey();
        }

        public static void StartAsyncServer()
        {
            //IPAddress localAddress = IPAddress.Parse("127.0.0.1");
            IPAddress localAddress = IPAddress.Parse("169.254.86.218");
            Int32 port = 13001;

            IPEndPoint ipLocal = new IPEndPoint(localAddress, port);
            aSyncListener = new TcpListener(ipLocal);
            aSyncListener.Start();
            
            WaitForASyncClientConnection();

        }

        private static void WaitForASyncClientConnection()
        {
            object obj = new object();

            aSyncListener.BeginAcceptTcpClient(new System.AsyncCallback(OnASyncClientConnect), obj);
            //IAsyncResult res = aSyncListener.BeginAcceptTcpClient(OnASyncClientConnect, aSyncListener);
            

        }
        private static void OnASyncClientConnect(IAsyncResult aSyncConnection)
        {
            try
            {
                TcpClient clientSocket = default(TcpClient);
                clientSocket = aSyncListener.EndAcceptTcpClient(aSyncConnection);

                ASyncClientRequest clientRequest = new ASyncClientRequest(clientSocket);
                clientRequest.StartClient();
                if (!connectedClients.Contains(clientRequest))
                {
                    connectedClients.Add(clientRequest);
                }
            }
            catch(Exception e)
            {
                throw e;
            }

            WaitForASyncClientConnection();
        }

        private static void StartSyncServer()
        {
            TcpListener server = null;
            try
            {
                // Set the TcpListener on port 13001.
                Int32 port = 13001;
                IPAddress localAddr = IPAddress.Parse("127.0.0.1");

                // TcpListener server = new TcpListener(port);
                server = new TcpListener(localAddr, port);

                // Start listening for client requests.
                server.Start();

                // Buffer for reading data
                Byte[] bytes = new Byte[256];
                String data = null;

                // Enter the listening loop. 
                while (true)
                {
                    Console.Write("Waiting for a connection... ");

                    // Perform a blocking call to accept requests. 
                    // You could also user server.AcceptSocket() here.

                    TcpClient client = server.AcceptTcpClient();


                    Console.WriteLine("Connected!");

                    data = null;

                    // Get a stream object for reading and writing
                    NetworkStream stream = client.GetStream();

                    int i;
                    Player tempPlayer = new Player(client);
                    if (!onlinePlayers.Contains(tempPlayer))
                    {
                        onlinePlayers.Add(tempPlayer);
                    }

                    // Loop to receive all the data sent by the client. 
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        // Translate data bytes to a ASCII string.
                        data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        Console.WriteLine("Received: {0}", data);

                        // Process the data sent by the client.
                        data = data.ToUpper();

                        byte[] msg = System.Text.Encoding.ASCII.GetBytes(data);

                        // Send back a response.
                        stream.Write(msg, 0, msg.Length);
                        Console.WriteLine("Sent: {0}", data);
                    }

                    // Shutdown and end connection
                    client.Close();
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                // Stop listening for new clients.
                server.Stop();
            }
        }

        private void CreateOpenGame(Player player)
        {

        }
        private void CloseGame(ActiveGame game)
        {

        }
        private void SendInfoToLogin(ActiveGame game)
        {

        }
        private void StartGame(ActiveGame game) //Threading
        {

        }
        private void UpdateGames(List<ActiveGame> openGames)
        {

        }
    }
}
